// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.hiru2008pythonfunction = require("./hiru2008python/function.js").handler;
exports.hiru2008pythonhirutest = require("./hiru2008python/hirutest.js").handler;